import { HttpClient, provideHttpClient } from '@angular/common/http';
import { APP_INITIALIZER, Type } from '@angular/core';
import { bootstrapApplication } from '@angular/platform-browser';
import { provideRouter, ResolveFn, Router, Routes } from '@angular/router';
import { firstValueFrom, tap } from 'rxjs';
import { AppComponent } from './app/app.component';
import { RouteConfigService } from './app/core/route-config.service';
import HomeComponent from './app/features/home.component';
import NotFoundComponent from './app/features/notFound.component';
import Page1Component from './app/features/page1.component';
import Page2Component from './app/features/page2.component';
import Page3Component from './app/features/page3.component';

const componentMap: {[key: string]: Type<any>}= {
  'features/home': HomeComponent,
  'features/page1': Page1Component,
  'features/page2': Page2Component,
  'features/page3': Page3Component,
};

export function initializeApp(http: HttpClient, routeConfigService: RouteConfigService, router: Router) {
  return (): Promise<any> =>
    firstValueFrom(
      routeConfigService.getRouteConfig()
        .pipe(
          tap((routesConfig) => {
            const dynamicRoutes: Routes = routesConfig.map(route => ({
                path: route.path,
                //loadComponent: () => import(`./${route.component}`).then(m => m.default)
                component: componentMap[route.component] || NotFoundComponent
              }));
            // dynamicRoutes.push({ path: '**', component: NotFoundComponent });
            router.resetConfig(dynamicRoutes);
          }),
          tap(() => {
            document.querySelector('.hello')!.innerHTML = 'miao';
          })
        )
    )
}

bootstrapApplication(AppComponent, {
  providers: [
    provideRouter([]),
    provideHttpClient(),
    {
      provide: APP_INITIALIZER,
      useFactory: initializeApp,
      multi: true,
      deps: [HttpClient, RouteConfigService, Router],
    },
  ],
});


