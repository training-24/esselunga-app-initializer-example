import { ChangeDetectionStrategy, Component, signal, Type } from '@angular/core';
import { Router, RouterLink, RouterOutlet, Routes } from '@angular/router';
import { firstValueFrom } from 'rxjs';
import { RouteConfigService } from './core/route-config.service';

@Component({
  selector: 'app-root',
  standalone: true,
  template: `
    <nav>
      <a routerLink="/home">Home</a>
      <a routerLink="/page1">page 1</a>
      <a routerLink="/page2">page 2</a>
    </nav>
    <router-outlet></router-outlet>

  `,
  imports: [
    RouterOutlet,
    RouterLink
  ]
})
export class AppComponent {
  constructor(public router: Router) {
    console.log(router.config);
  }

}
