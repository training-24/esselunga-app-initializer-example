// src/app/services/route-config.service.ts
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

type MyRoute = {path: string, component: string}
@Injectable({
  providedIn: 'root'
})
export class RouteConfigService {
  private configUrl = 'http://localhost:3001/config'; // URL to your JSON file
  constructor(private http: HttpClient) {}

  getRouteConfig(): Observable<MyRoute[]> {
    return this.http.get<MyRoute[]>(this.configUrl);
  }
}
