import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-panel',
  standalone: true,
  template: `
    {{counter}}
`,
})
export class PanelComponent {
  @Input() counter = 0
}
